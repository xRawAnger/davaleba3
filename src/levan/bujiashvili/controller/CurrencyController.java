package levan.bujiashvili.controller;

import levan.bujiashvili.currency.Currency;

import java.awt.*;

@Path("/CurrencyManager")
public class CurrencyController {
    @GET
    @Path("/currencies")
    @Produces(PageAttributes.MediaType.APPLICATION_JSON)
    public List<Currency> getCurrency(){
        List<Currency> currenciesList = new ArrayList<>();
        currenciesList.add(new Currency("USD", 3.16, 3.2));
        currenciesList.add(new Currency("EUR", 3.42, 3.47));
        currenciesList.add(new Currency("GBP", 3.84, 3.94));
        currenciesList.add(new Currency("RUB", 4.12, 4.25));
        currenciesList.add(new Currency("TRY", 0.4, 0.47));
        currenciesList.add(new Currency("AZN", 1.6, 1.84));
        return currenciesList;
    }
}
