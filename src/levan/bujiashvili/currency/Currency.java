package levan.bujiashvili.currency;

public class Currency {
    private String title;
    private double course;
    private double sell;

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }


    public Currency() {
    }

    public Currency(String title, double course) {
        this.title = title;
        this.course = course;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "title='" + title + '\'' +
                ", course=" + course +
                ", sell=" + sell +
                '}';
    }
}
